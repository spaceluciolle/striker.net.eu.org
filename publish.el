(add-to-list 'load-path "~/.emacs.d/.local/straight/repos/emacs-htmlize/")
(require 'ox-publish)
(require 'htmlize)
(setq org-html-htmlize-output-type 'css)
(setq org-html-validation-link nil)

(setq org-publish-project-alist
      '(
        ("org-notes"
         :base-directory "post/"
         :base-extension "org"
         :publishing-directory "public/"
         :recursive t
         :publishing-function org-html-publish-to-html
         :headline-levels 4
         :auto-preamble t
         )
        ("org-static"
         :base-directory "post/"
         :base-extension "css\\|js\\|png\\|jpg\\|gif\\|pdf\\|mp3\\|ogg\\|swf"
         :publishing-directory "public/"
         :recursive t
         :publishing-function org-publish-attachment
         )
        ("org" :components ("org-notes" "org-static"))
        ))
